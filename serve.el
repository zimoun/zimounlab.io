;; simple way to serve

(require 'simple-httpd)
(setq httpd-root "."
      httpd-port 8080)
(httpd-start)
(message "http://localhost:%s" httpd-port)
