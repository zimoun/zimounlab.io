
# clean
[[ -d public/ ]] && rm -fr public
[[ -d tmp/ ]] && rm -fr tmp

CSS="sassc -I css src/css/org.scss > src/css/org.css"
POSTS="emacs -q --batch -l publish.el --eval='(org-publish \"site\")'"

# build pages
guix time-machine -C channels.scm      \
     -- environment -C -m manifest.scm \
     -- sh -c "$CSS; $POSTS;"

# build derivate
DIR="src/posts/2019-03-04-derivee-dual"
guix time-machine -C $DIR/channels.scm                \
     -- environment -C -m $DIR/manifest.scm           \
     -- emacs --batch  -l publish.el "$DIR/index.org" \
     -f org-babel-execute-buffer                      \
     -f save-buffer-with-message

guix time-machine -C $DIR/channels.scm             \
     -- environment -C -m $DIR/manifest.scm        \
     -- emacs --batch  -l publish.el               \
     --eval="(org-publish-project \"derivative\")" \
     --eval="(org-publish-project \"extra\")"
mkdir -p public/posts/2019-03-04-derivee-dual/
mv tmp/* public/posts/2019-03-04-derivee-dual/
rm -fr tmp
git checkout -- src/posts/2019-03-04-derivee-dual/index.org
