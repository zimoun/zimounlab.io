
# Generate Docker images
docker load \
       < $(guix time-machine -C channels.scm \
                -- pack                      \
                -f docker                    \
                -C none                      \
                -S /bin=bin                  \
                -S /lib=lib                  \
                -S /share=share              \
                -S /etc=etc                  \
                -m manifest.scm              \
                --save-provenance)

# Tag
docker tag <IMAGE ID> zimoun/blog:<my-version>

# Push
docker login --username=zimoun
docker push zimoun/blog:<my-version>

# Build
docker run -v `pwd`:`pwd` -w `pwd` -ti <TAG> \
       emacs --batch -l publish.el -f org-publish-all
