(define-module (sort-guile-naive)
  #:use-module (ice-9 format)
  #:export (run))

(include "./sort.scheme")
(include "./common.scm")


(define (run)
  (runner sort-merge-naive >=))
