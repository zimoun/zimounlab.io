;; -*- mode:scheme -*-

(include "./sort.scheme")

(suppress-greeting #t)

(scheme-start
 (lambda (arg)
   (let* ((k (string->number arg))
          (b 10)
          (n (power b k))
          (l (time (make-block n 5)))
          (m (time (length l)))
          (n* (time (len l)))
          (ok? (eqv? n* n))
          (s (time (sort-merge-naive l >=)))
          (is-sorted? (time (re-sorted? s >=)))
          )
     (printf "OK? ~s~%"
             (and ok?
                  is-sorted?
                  )))))
