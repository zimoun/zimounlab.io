(define-module (sort-guile-record-builtin)
  #:use-module (ice-9 format)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-43)
  #:export (run))

(include "./sort.scheme")
(include "./common.scm")
(include "./common-record.scm")


(define (run)
  (runner sort >=-elem? make-elems))
