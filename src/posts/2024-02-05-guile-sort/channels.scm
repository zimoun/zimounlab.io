(list (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (branch "master")
        (commit
          "8e61e6351510f5665d09c6debc0584b3ed218e73")))
