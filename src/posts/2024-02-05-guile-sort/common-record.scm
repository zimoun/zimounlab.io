

(define-record-type <elem>
  (make-elem num stuff)
  elem?
  (num elem-num)
  (stuff elem-stuff))

(define (make-elems len block)
  (let loop ((n len) (k block) (accu '()))
    (if (= n 0)
        accu
        (let ((elem (make-elem (- n k)
                                   (list->vector
                                    (map (lambda (x) (+ n k x))
                                         (iota (+ 1 k)))))))
          (if (= k 0)
              (loop (- n 1) block (cons elem accu))
              (loop (- n 1) (- k 1) (cons elem accu)))))))

(define (>=-elem? e1 e2)
  (let ((n1 (+ (elem-num e1) (vector-fold (lambda (i r x) (+ x r))
                                           0
                                           (elem-stuff e1))))
        (n2 (+ (elem-num e2) (vector-fold (lambda (i r x) (+ x r))
                                           0
                                           (elem-stuff e2)))))
    (>= n1 n2)))
