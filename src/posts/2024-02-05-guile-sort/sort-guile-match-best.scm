(define-module (sort-guile-match-best)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:export (run))

(include "./sort.scheme")
(include "./common.scm")



(define (run)
  (runner sort-merge-match >=))


(define (fold* proc init lst)
  (let fold-loop ((lst lst) (accu init))
    (match lst
      ((x tl ...)
       (fold-loop tl
                  (proc x accu)))
      (_
       accu))))

(define (len* lst)
  (fold* (lambda (_ n) (+ 1 n)) 0 lst))

(define (rev-append* l1 l2)
  (fold* cons l2 l1))

(define (re-sorted*? lst less?)
  (let loop ((lst lst))
    (match lst
      ((x y tl ...)
       (and (less? x y)
           (loop (cons y tl))))
      (_
       #true))))


(define (sort-merge-match lst less?)

  (define (rev-merge l1 l2 less?)
    (let r-m-loop ((l1 l1) (l2 l2) (accu '()))
      (match l1
        ((x1 tl1 ...)
         (match l2
           ((x2 tl2 ...)
            (if (less? x1 x2)
                (r-m-loop tl1 l2
                          (cons x1 accu))
                (r-m-loop l1 tl2
                          (cons x2 accu))))
           (_
            (rev-append* l2 accu))))
        (_
         (rev-append* l2 accu)))))

  (define (rev-merge-rev l1 l2 less?)
    (let r-m-r-loop ((l1 l1) (l2 l2) (accu '()))
      (match l1
        ((x1 tl1 ...)
         (match l2
           ((x2 tl2 ...)
            (if (less? x2 x1)
                (r-m-r-loop tl1 l2
                          (cons x1 accu))
                (r-m-r-loop l1 tl2
                          (cons x2 accu))))
           (_
            (rev-append* l2 accu))))
        (_
         (rev-append* l2 accu)))))

  (letrec ( ;;; Two mutally recursive sort / rev-sort
           (sort
            (lambda (n lst)
              (cond ((= 2 n)
                     (match lst
                       ((x1 x2 tl ...)
                        (if (less? x1 x2)
                            (cons
                             (list x1 x2) tl)
                            (cons
                             (list x2 x1) tl)))))
                    ((= 3 n)
                     (match lst
                       ((x1 x2 x3 tl ...)
                        (if (less? x1 x2)
                            (if (less? x2 x3)
                                (cons
                                 (list x1 x2 x3) tl)
                                (if (less? x1 x3)
                                    (cons
                                     (list x1 x3 x2) tl)
                                    (cons
                                     (list x3 x1 x2) tl)))
                            (if (less? x1 x3)
                                (cons
                                 (list x2 x1 x3) tl)
                                (if (less? x2 x3)
                                    (cons
                                     (list x2 x3 x1) tl)
                                    (cons
                                     (list x3 x2 x1) tl)))))))
                    (else
                     (let* ((n1 (if (even? n) ;How to bit-shift-right of 1?
                                    (/ n 2)
                                    (/ (- n 1) 2)))
                            (n2 (- n n1))
                            (s1 (rev-sort n1 lst))
                            (s2 (rev-sort n2 (cdr s1))))
                       (cons
                        (rev-merge-rev (car s1) (car s2)
                                       less?)
                        (cdr s2)))))))

           (rev-sort
            (lambda (n lst)
              (cond ((= 2 n)
                     (match lst
                       ((x1 x2 tl ...)
                        (if (less? x2 x1) ;reverse
                            (cons
                             (list x1 x2) tl)
                            (cons
                             (list x2 x1) tl)))))
                    ((= 3 n)
                     (match lst
                       ((x1 x2 x3 tl ...)
                        (if (less? x2 x1) ;reverse
                            (if (less? x3 x2)
                                (cons
                                 (list x1 x2 x3) tl)
                                (if (less? x3 x1)
                                    (cons
                                     (list x1 x3 x2) tl)
                                    (cons
                                     (list x3 x1 x2) tl)))
                            (if (less? x3 x1)
                                (cons
                                 (list x2 x1 x3) tl)
                                (if (less? x3 x2)
                                    (cons
                                     (list x2 x3 x1) tl)
                                    (cons
                                     (list x3 x2 x1) tl)))))))
                    (else
                     (let* ((n1 (if (even? n) ;XXXX: bit shift
                                    (/ n 2)
                                    (/ (- n 1) 2)))
                            (n2 (- n n1))
                            (s1 (sort n1 lst))
                            (s2 (sort n2 (cdr s1))))
                       (cons
                        (rev-merge (car s1) (car s2)
                                   less?)
                        (cdr s2)))))))

           ;; Last letrec: compute once the length of list
           (n (len* lst)))
    (if (< n 2)
        lst
        (match (sort n lst)
          ((result rest ...)
           result)))))
