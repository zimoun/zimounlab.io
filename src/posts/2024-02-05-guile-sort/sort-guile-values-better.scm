(define-module (sort-guile-values-better)
  #:use-module (ice-9 format)
  #:export (run))

(include "./sort.scheme")
(include "./common.scm")

(define (run)
  (runner sort-merge-values >=))
