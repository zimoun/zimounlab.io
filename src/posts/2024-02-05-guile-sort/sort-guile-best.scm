(define-module (sort-guile-best)
  #:use-module (ice-9 format)
  #:export (run))

(include "./sort.scheme")
(include "./common.scm")

(define (run)
  (runner sort-merge* >=))
