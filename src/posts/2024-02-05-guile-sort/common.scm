
(define-syntax time
  (syntax-rules ()
    ((_ (proc args ...))
     (let* ((diff (lambda (start end)
                    (/ (- end start) 1.0 internal-time-units-per-second)))
            (gc-start (gc-run-time))
            (real-start (get-internal-real-time))
            (run-start (get-internal-run-time))
            (result (proc args ...))
            (run-end (get-internal-run-time))
            (real-end (get-internal-real-time))
            (gc-end (gc-run-time)))
       (format #t ";; ~,6Fs real time, ~,6Fs run time.  ~,6Fs spent in GC.~%"
               (diff real-start real-end)
               (diff run-start run-end)
               (diff gc-start gc-end))
       result))))

(define* (runner sorter cmp?
                 #:optional (maker make-block))
  (let* ((k (string->number (cadr (command-line))))
         (b 10)
         (p 5)
         (n (power b k))
         (show (pk 'start-block b k p))
         (l (time (maker n p)))
         (show (pk 'start-len b k))
         (m (time (length l)))
         (n* (time (len l)))
         (ok? (eqv? n* n))
         (show (pk 'start-sort b k))
         (s (time (sort l cmp?)))
         (show (pk 'start-sorted? b k))
         (is-sorted? (time (re-sorted? s cmp?)))
         )
    (format #t "OK? ~s~%" (and ok?
                               is-sorted?
                               ))))

