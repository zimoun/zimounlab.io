;; -*- mode:scheme -*-
#lang racket

(provide run)

(include "./sort.scheme")

(define (run)
  (let* ((k (string->number (car (vector->list
                                  (current-command-line-arguments)))))
         (b 10)
         (n (power b k))
         (l (time (make-block n 5)))
         (m (time (length l)))
         (n* (time (len l)))
         (ok? (eqv? n* n))
         (s (time (sort l >=)))
         (is-sorted? (time (re-sorted? s >=) ))
         )
    (printf
     "T: divide by 1000~% + make-block~% + length~% + len~% + sort~% + sorted?~%OK? ~s~%"
     (and ok?
          is-sorted?
          ))))
