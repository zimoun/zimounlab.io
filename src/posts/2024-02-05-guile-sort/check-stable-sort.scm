(use-modules (ice-9 format)
             (srfi srfi-9)
             ((srfi srfi-1) #:select (reduce-right))
             )

(define-record-type <elem>
  (make-elem num value)
  elem?
  (num elem-num)
  (value elem-value))

(define (make-elems len len-block)
  (let loop ((n len) (k len-block) (accu '()))
    (if (= n 0)
        accu
        (let* ((elem (make-elem n 10))
               (new (cons elem accu))
               (n-1 (- n 1)))
          (if (= k 0)
              (loop n-1 len-block new)
              (loop n-1 (- k 1) new))))))

(define (>=-elem? e1 e2)
  (>= (elem-value e1)
      (elem-value e2)))

(define (ordered? lst)
  (and
   (reduce-right (lambda (e r)
                   (and r
                        (let ((n (elem-num e))
                              (m (elem-num r)))
                          (if (< n m)
                              e
                              #false))))
                     #false
                     lst)
   #true))
