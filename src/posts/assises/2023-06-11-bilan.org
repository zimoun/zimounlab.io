# -*- mode: org ; coding: utf-8 -*-
#+STARTUP: showeverything

#+TITLE: Juré aux Assises, que m'en reste-t-il ?
#+AUTHOR: simon
#+date: <2023-06-11 Sun>
#+LANGUAGE: fr

#+SETUPFILE: ../../templates/style.org
# #+SETUPFILE: ../templates/code.org
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../../css/org.css" />

#+OPTIONS: num:nil toc:nil



Une Cour d'Assises, c'est toujours de la souffrance.  Évidemment, on est
emporté par la sympathie, la compassion et l’empathie – souffrir avec ou
ressentir la souffrance à la place de.  Au-delà des charges émotionnelles qui
imposent l’humilité, au-delà des quatre jours de procès auquel j'ai assisté,
primo comme juré numéro 6 et ensuite comme juré supplémentaire numéro 2,
finalement que me reste-t-il ?

*D’abord*, une nouvelle adhésion au pacte citoyen.  Le dernier devoir citoyen
est la fonction de juré – n’appelons pas la Journée d’Appel à la Défense
(JAPD) un devoir quand cette journée ressemble surtout à l’évaluation du
niveau d’analphabétisme d’une génération.  Lors ces jours de procès, j’ai
souvent pensé à la devise Liberté, Égalité, Fraternité.

La Liberté, au cœur de la réponse pénale ; la /prison/ est la privation de la
liberté d’aller et venir.  Le [[https://www.cglpl.fr/][Contrôleur Général des Lieux de Privation de
Liberté]] (CGLPL) établit année après année que la /prison/ est bien plus qu’une
simple privation de la liberté d’aller et venir.  Peut-on se satisfaire d’une
peine qui prive de liberté sans définir de quelles libertés il est question ?
Peut-on se satisfaire d’une loi d'airain qui pèse sur la condition carcérale ?
Cette loi implacable dont on ne saurait échapper : en /prison/, la vie doit être
plus difficile que celle du travailleur libre le plus défavorisé.  Pourrait-on
admettre une alternative ?

L’Égalité, c'est l’huile des rouages.  Par exemple, neuf personnes voteront
dans la chambre des délibérations et toutes les voix sont égales.  Cependant,
on peut poser la question de l’égalité de traitement.  Aussi impartial que
l’on cherche à l’être, il faut reconnaître certains biais qui se jouent par
devers.  Ceux qui passent à la barre sont scrutés, et même si on s’y refuse,
il faut dire que la posture, la voix, le vocabulaire, le regard, l’allure, la
manière de répondre, tout cela a une influence sur la réception de ce qui est
dit, ou parfois pas dit.  Étant six jurés, trois magistrats professionnels et
des jurés supplémentaires, ce biais naturel tend à s’atténuer mais peut-on
réellement incarner cette Égalité ?  Du moins, elle l’est autant que possible
par le dispositif abstrait.  Est-ce suffisant pour la concrétiser dans le
système pénal ?  De la Police à la Justice en passant par l’Administration
Pénitentiaire.

La Fraternité, cette petite sœur trop souvent oubliée.  C’est ce qui m’a le
plus marqué dans cette expérience citoyenne  : l’élan fraternel.  Quoiqu’on
dise sur la Justice et les professionnels du Droit, ce que j’ai vu c’est
d’abord une humanité.  Il y a des tas d’exemples sur la machine judiciaire qui
broie, ici j’ai surtout vu des acteurs du procès qui écoutent, qui cherchent à
démêler les nœuds, qui prennent le temps qu’il faut.

La Fraternité est, me semble-t-il, même au cœur du juré populaire : « je te
juge comme je jugerais mon frère ».  J’ai vu des jurés concentrés sur les
débats remplissant des pages de notes.  J’ai vu des jurés concernés et
impliqués.  J’ai vu des jurés jonglant avec leurs impératifs pour accomplir
leur devoir au mieux – qui paie la nounou quand il n’y a pas d’heure de fin
d’une journée ?  J’ai vu des jurés qui respectent la différence.  J’ai vu des
jurés qui s’écoutent les uns les autres.  J’ai vu une démonstration de
l'intelligence collective.  Malgré toutes les critiques sur la Justice d’une
Cour d’Assises, j’ai vu la mesure.  Avant le procès, j’étais circonspect sur
la capacité de ne pas succomber aux sirènes de l’émotion, et bien non, je ne
l’ai pas vu.  Est-ce que le délibéré correspondait à mon /intime conviction/
 ?  Peut-être que oui, peut-être que non, peu importe.  L’important est que
j’ai été témoin d’une intelligence collective qui a permis à neuf personnes de
s’accorder.  Que le Président de la Cour y ait joué un rôle notable, sûrement.
Que les deux Assesseurs aussi, probablement.  Surtout le groupe a agi avec
sincérité, clairement.

*Ensuite*, je retiens l’opposition entre rationalité et cohérence.  L’univers
des livres Le Seigneur des Anneaux est cohérent et pourtant il n’est pas
rationnel ; personne n’a jamais vu un Hobbit, ni des Elfes immortels ou un
Magicien comme le personnage Gandalf.  Ce n’est pas la rationalité qui fait
adhérer à l’histoire, non c’est la cohérence interne.  Maintenant si le robot
Terminator débarque dans cet univers, il y a une perte de cohérence.  Il
serait faux de penser que les débats d’un procès d’Assises se placent au
niveau de la rationalité ou de la logique.  Qu’est-ce qui pourrait être
logique ou rationnel dans une tentative de meurtre ?  Ou dans un viol
collectif ? À la place de la rationalité ou de la logique se substitue la
cohérence.  Et pour que cette cohérence s’anime, il faut de la crédibilité.
L’approximation est un voile empêchant d’accéder à cette crédibilité.

Je dois avouer que j’ai été aussi interpellé par les approximations qui se
cumulent tout au long de la procédure.  Peut-être de par mon métier dans la
recherche scientifique je suis habitué à plus de rigueur (et pourtant je
pourrais parler des heures sur les difficultés dans la recherche
– reproductibilité entre autres).  Un procès en Cour d’Assises, c’est un
puzzle avec des pièces manquantes, des pièces déformées, des pièces qui se
contredisent et ne s’emboîtent pas, alors autant évacuer le plus possible ce
qui est factuellement vérifiable sans recourir à une parole.  Un exemple ?  Un
témoin dit que la distance entre deux carrefours est de quinze mètres ; un
autre dit que cette distance est de ving-cinq mètres.  Les avocats des
Accusés, les avocats de la Partie Civile et l’Avocat Général s’excitent un
peu ; l’un prend ce qui l’arrange, l’autre cherche à déconstruire la
crédibilité du reste du témoignage, le dernier soupire.  Si cette distance est
d’une importance fondamentale, il suffit de la mesurer, non ?  Et cette mesure
est facilement vérifiable.  Autrement, naviguer sur l’approximation est du
verbiage de Cour irrespectueux des témoins, des jurés et surtout de l’Accusé
et du Plaignant.  Un autre exemple ?  Dans un message court (SMS) entre
jeunes, le terme « ZBI » apparaît.  Et voilà que ça pérore sans réellement en
donner la signification alors que le premier dictionnaire en ligne y répond ;
terme tellement mystérieux que la [[https://fr.wiktionary.org/wiki/z%C3%A9bi][forme orginale]] est même utilisée[fn:1:« À
ces moments-là, l’impératif catégorique, l’existentialisme et tous les livres
de Simone de Beauvoir me sont [[https://fr.wiktionary.org/wiki/peau_de_z%C3%A9bi][peau de zébi]]. »] par [[https://fr.wikipedia.org/wiki/Annie_Ernaux][Annie Ernaux]], Prix Nobel de
littérature.  Oui, je suis surpris par les approximations dans la procédure
qui s’étale sur plusieurs mois avec plusieurs intervenants.  Mon impression
diffuse – et peut-être prompte – est que la procédure ne va pas toujours au
bout.  Se satisfait-elle de certaines inconsistances ?  Ou manque-t-elle de
moyens ?  Ou les deux ?

En quelque sorte, il y a un matériau – des témoignages, des enquêtes, des
expertises – et les quatre acteurs du procès – la Cour et les jurés, l’Avocat
Général, la Partie Civile et l’Accusé – tentent tous d’écrire une histoire
cohérente avec ce même matériau.  Et c’est là que les accusés ou les
plaignants s’enferment parfois dans une mauvaise stratégie.  En particulier,
les accusés préfèrent parfois opposer à une spécificité à charge une
explication rationnelle quitte à déformer la cohérence.  Un tel stratagème
prend le risque d’entacher la crédibilité et donc l’adhésion par cohérence.
Sans oublier qu’[[https://fr.wikipedia.org/wiki/Rasoir_d%27Ockham][Ockham rase de près]] – le principe de simplicité prévaut – les
hypothèses suffisantes les plus simples doivent être préférées.  La rhétorique
dans les débats est un bouclier efficace mais la perte de cohérence transperce
tout.  Et évidemment, ce n’est pas parce que c’est compatible que cela
signifie que c’est arrivé comme le laisserait entendre ladite compatibilité.
La boussole reste la cohérence construite sur la sincérité de la preuve.  La
sincérité est l’unique source indéfectible de cohérence.

*Enfin*, je repars avec des questions.  Par exemple, si j’ai bien compris, le
Droit Pénal se place du point de vue de l’auteur et pas de la victime.
Autrement dit, dans le cas d’un viol, la victime peut ne pas être consente
mais l’auteur peut ne pas être capable de l’apprécier ; la notion d’intention
est difficile à évaluer.  Autre exemple, la tentative de meurtre.  L’Article
[[https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006417210][121-5]] du Code Pénal définit la tentative comme :

#+begin_quote
La tentative est constituée dès lors que, manifestée par un commencement
d'exécution, elle n'a été suspendue ou n'a manqué son effet qu'en raison de
circonstances indépendantes de la volonté de son auteur.
#+end_quote

Et considérons deux cas.  Le premier, une personne qui attaque avec un couteau
et pique une dizaine de fois au niveau des fesses en clamant haut et fort « Je
vais te tuer ! ».  Le deuxième, une personne dans une bagarre à main nue sort
le même couteau et plante un opposant dans le ventre.  Disons que les victimes
ne sont pas mortes, dans le premier cas parce que les fesses ne sont pas
vitales, dans le deuxième parce que les secours sont intervenus à temps ; dans
les deux cas, le commencement d'exécution a manqué son effet en raison de
circonstances indépendantes de la volonté de l’auteur.  Ces deux cas sont-ils
des tentatives de meurtre ?  Dans le premier l’intention est claire mais pas
la caractérisation et dans le second la caractérisation est claire mais pas
l’intention.  La Doctrine du Droit doit répondre à ces questions, j’imagine.
Cependant le néophyte que je suis a dû se les poser, se les pose encore…

Le procès de tentative de meurtre et celui de viol – non-consentement – soulèvent
la même question : celle de la volonté.  C’est le banc de celui à qui les
débats la posent qui change.

*Au final*, je retiens qu’ « un ego blessé vaut mieux qu’une tragédie. »
