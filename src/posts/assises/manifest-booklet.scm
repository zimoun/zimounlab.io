;; What follows is a "manifest" equivalent to the command line you gave.
;; You can store it in a file that you may then pass to any 'guix' command
;; that accepts a '--manifest' (or '-m') option.

(specifications->manifest
  (list "texlive-extsizes"

        "texlive-ebgaramond"
        "texlive-fontspec"

        "texlive-geometry"
        "texlive-pdfpages"
        "texlive-pdflscape"             ;required by pdfpages
        "texlive-lettrine"
        "texlive-xkeyval"               ;required by lettrine
        "texlive-minifp"                ;required by lettrine

        "texlive-pgfornament"
        "texlive-pgfopts"               ;required by pgfornament

        "texlive-scheme-basic"
        "texlive-ec"
        "texlive-kpfonts"
        "texlive-cm-super"
        "texlive-amsfonts"
        "texlive-beamer"
        "texlive-translator"
        "texlive-ulem"
        "texlive-capt-of"
        "texlive-hyperref"
        "texlive-carlisle"
        "texlive-wrapfig"
        "texlive-amsmath"
        "texlive-listings"
        "texlive-geometry"
        "texlive-babel-french"
        "texlive-fancyvrb"
        ))
