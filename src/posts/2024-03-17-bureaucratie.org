# -*- mode: org ; coding: utf-8 -*-
#+STARTUP: content

#+TITLE: Billet d’humeur sur la bureaucratie
#+SUBTITLE: De la bureaucratie à la convivialité
#+AUTHOR: simon
#+date: <2024-03-17 Sun>
#+LANGUAGE: en

#+SETUPFILE: ../templates/style.org
# #+SETUPFILE: ../templates/code.org
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../css/org.css" />

#+OPTIONS: num:nil toc:nil



Je lis régulièrement le journal hebdomadaire [[https://le1hebdo.fr][Le 1 hebdo]].  Le numéro 487 du 13
mars 2024 est consacré à [[https://le1hebdo.fr/journal/numeros/487/comment-la-bureaucratie-prend-le-pouvoir.html][Comment la bureaucratie prend le pouvoir]].  La
Bureaucratie, vaste sujet !  Et en tant que fonctionnaire titulaire, je ne
suis pas seulement confronté à l'Administration Centrale comme citoyen, je le
suis aussi en tant qu’employé de l'État.  Cette double confrontation ne
justifie que mon intérêt et curiosité à comprendre ce que signifie réellement
une Bureaucratie.

Restant sur ma faim après la lecture du numéro, je me suis donc permis
d’écrire à la Rédaction.  Ceci fait, je ne reste pas sur une simple critique
(peut-être) facile ; n'est-ce pas l’art qui est difficile ? Alors par un
manque flagrant d’humilité, j’ose me risquer à l’exercice : écrire la [[https://le1hebdo.fr/journal/comment-la-bureaucratie-prend-le-pouvoir/487/article/de-kafka-au-techno-cocon-6452.html][section
« Zoom »]] que j’aurais aimé lire.

La contrainte semble d’environ 400 mots.  Voilà mon brouillon ; je le pose là
comme une feuille volante.  Mon ego rend public cette simple trace pour mon
futur moi. Hum?! est-ce que ce brouillon ne ressemble pas à un Édito… ou un
billet d’humeur ?

#+HTML: <br><hr><br>


La Bureaucratie est un ensemble de procédés pour réaliser une tâche
(administrative).  Autrement dit, la Bureaucratie relève du domaine de la
Technique.  Les techniques ne concernent pas uniquement l’automatisation par
la machine, on parle tout aussi bien des techniques publicitaires, des
techniques d’écriture ou du geste technique du sportif.  Ce qui se joue à la
racine de la technique est la recherche d’efficacité, au sens de la recherche
d’un gain comparé à une situation donnée.  Vendre plus, écrire de meilleures
histoires, marquer des buts. Une Technique est la codification de procédés qui
se veulent optimaux.  La Bureaucratie entre dans ce cadre.  La norme ou le
formulaire ne sont que des instruments de cette codification : la norme
cherche à régler une situation et le gain signifie la santé publique ou la
réduction d’accidents sur un chantier ; et le formulaire encadre pour gagner
du temps sur son traitement.

Mais quel est le problème alors ?  Le problème s’exprime dans le paradoxe
suivant : d’un côté, la Bureaucratie se construit par la recherche
d’efficacité (gain) et de l’autre les conséquences de cette recherche
produisent un tout inefficace (perte).  Ce paradoxe se retrouve dans beaucoup
de mise en association de techniques, comme mis en relief par [[https://fr.wikipedia.org/wiki/Bernard_Charbonneau][Bernard
Charbonneau]] (/Le système et le chaos : Critique du développement
exponentiel/, 1973).  La question se cristallise alors autour de pourquoi ou
comment ce paradoxe apparaît-il ?  [[https://fr.wikipedia.org/wiki/Jacques_Ellul][Jacques Ellul]] dessine une réponse ([[https://fr.wikipedia.org/wiki/Le_Syst%C3%A8me_technicien][/Le
Système Technicien/]], 1977) en caractérisant le système par : son autonomie,
son unité, son universalité et sa totalisation.

La Bureaucratie n’est pas une technocratie : ce n’est pas la technique en soi
qui dirige, ni complètement un petit groupe restreint d’experts techniciens.
La Bureaucratie est l’expression d’un système (technique) qui passe avant
l’individu (autonome), qui standardise excluant l’exception (unité), qui
s’applique à tous et pour tout (universalité) et qui se propage de services en
services (totalisation).  L’imaginaire explore les limites extrêmes de tels
systèmes exagérant l’une ou l’autre caractéristique, de Orwell (/1984/, 1949)
à Gaiman et Pratchett (/Good Omens/, 1990) en passant par Gilliam
(/Brazil/, 1985) ou le comics /Loki/.

Comment sortir de l’impasse ? La « [[https://fr.wikipedia.org/wiki/Outil_convivial][/convivialité/]] », peut-être. Prolongation
du concept défini par [[https://fr.wikipedia.org/wiki/Ivan_Illich][Ivan Illich]] (/La Convivialité/, 1973), peut-être.
Comment considérer notre infrastructure d’administration comme un [[https://fr.wikipedia.org/wiki/Bien_commun][bien commun]]
pérenne ?  Osons l’application stricte de la transparence et de la
souveraineté (maîtrise internalisée de la technique).  Cela pourrait commencer
par un [[https://code.gouv.fr/sill/readme][socle inter-ministériel]] d’outils informatiques transparents et
souverains.  Non pas une sur-couche ou une rustine à la situation actuelle
mais repenser en profondeur notre relation à la Technique et ses enjeux.  En
somme, transformer la Bureaucratie en système convivial.  Tout un programme…
technique !

#+begin_comment

#+HTML: <br><hr><br>

Bonjour,

Avant tout, je vous remercie pour la qualité des numéros ; sans s'essouffler
avec le temps et se réinventant pour aider à regarder le monde différemment et
donc mieux le saisir ou le penser autrement.

Ceci étant dit, je suis étonné à la lecture de votre numéro 487 par l'absence
de mention de Jacques Ellul ou Bernard Charbonneau ou d'autres intellectuels
qui ont décrits avec une grande précision les enjeux de notre temps : notre
rapport à la technique.

Parler de Bureaucratie sans dessiner la piste d'un Système Technicien, c'est à
mon avis rater le tissage profond qui se joue et c'est rester à la surface de
ce nous dit la Bureaucratie.

À la question « Comment en est-on arrivé à cette prise de pouvoir de la
technostructure sur l’individu ? », Jacques Ellul fournit une réponse précise
et détaillée, dans « Le Système Technicien » par exemple.  Peu importe.  La
réponse ne se noue-t-elle pas dans notre rapport à la technique ?  La
technique est d’une part un instrument de pouvoir et d’autre part présente ce
paradoxe : nous avons vu la technisation de la société comme un moyen de
accéder à plus de libertés et finalement nous voilà à questionner les
conséquences de cette acquisition.

La Bureaucratie n’est qu’un exemple des conséquences de cette inter-connexion
des techniques.  C’est ce que j’entends par dessiner la piste du Système
Technicien : d’une part donner un cadre pour discuter l’arbitrage sur le thème
(la Bureaucratie) et d’autre part donner une grille de lecture qui ouvre en
filigrane à d’autres arbitrages comme le rapport paradoxal entre l’engouement
pour l’Intelligence Artificielle très consommatrice d’énergie et les enjeux
énergétiques du changement climatique.

En somme, un Zoom dans ce sens m’aurait semblé largement pertinent.  Et bien
plus pertinent que celui publié.

Bref, mon opinion ne vaut que cela, une opinion d'un lecteur.

Et donc ?  La prolongation de cette opinion devient une suggestion.  Un numéro
"Ce que nous dit" consacré à Jacques Ellul.  De mon point de vue, ce serait la
grille de lecture complémentaire au numéro "Ce que nous dit" Guy Debord.

En passant, c'est grâce à votre numéro "Ce que nous dit" Simon Weil que je
l'ai redécouverte et ensuite que je l'ai vraiment lue (en partie) .  Je vous
remercie d'avoir attisé ma curiosité.

Pour finir, merci pour votre journal qui nourrit intelligemment.

Bien cordialement, simon

#+end_comment
