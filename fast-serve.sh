
# clean
[[ -d public/ ]] && rm -fr public
[[ -d tmp/ ]] && rm -fr tmp

# fast build
[[ ! -d public/ ]] && ./fast-build.sh

# ease paths
[[ ! -d public/ ]] && mkdir -p public
cp serve.el public/

# fix issue with user-emacs-directory-warning
mkdir -p public/.emacs.d

# run http server
guix environment -C -N -m manifest.scm -E TERM \
     --no-cwd --expose=./public=$HOME          \
     -- emacs -Q -l $HOME/serve.el             \
     --eval '(switch-to-buffer "*httpd*")'
