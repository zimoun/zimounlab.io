;;; publish.el --- Config -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'ox-publish)
(require 'cl-lib)                       ;cl-reduce

(defun do-nothing (a b c)
  "TODO"
  nil)

(defun public (dir)
  (format "public/%s" dir))

(defun src (dir)
  (format "src/%s" dir))

(defvar header
  "<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/org.css\">
<a href=\"../index.html\">About</a>")

(setq
 make-backup-files            nil
 org-id-track-globally        nil
 org-html-htmlize-output-type 'css
 org-html-checkbox-type       'html
 org-html-html5-fancy         t
 org-html-doctype             "html5"
 org-confirm-babel-evaluate   nil
 org-link-file-path-type      'relative
 org-html-preamble            t
 org-html-postamble           t

 org-html-postamble-format
 '(("en" "<center><hr>
<p class=\"author\">© 2014-2024 Simon Tournier &lt;simon (at) tournier.info &gt;
<a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"><img style=\"border-width:0\" src=\"https://licensebuttons.net/l/by-sa/4.0/80x15.png\" /></a></p>
<p class=\"date\">(last update: %C)</p>
</center>"))

 org-publish-project-alist
 `(("bare-bone"
    :base-directory ,(src "")
    :base-extension "org"
    :publishing-directory ,(public "")
    :publishing-function org-html-publish-to-html
    :recursive nil
    :exclude ,(regexp-opt '("posts" "css" "README.org")))
   ("copy-file"
    :base-directory ,(src "")
    :base-extension "jpg\\|pdf"
    :publishing-directory ,(public "")
    :publishing-function org-publish-attachment
    :recursive t)
   ("css"
    :base-directory ,(src "css")
    :base-extension "css"
    :publishing-directory ,(public "css")
    :publishing-function org-publish-attachment
    :recursive t)
   ("extra"
    :base-directory ,(src "posts")
    :base-extension "c\\|py\\|R\\|scm\\|sh\\|txt"
    :publishing-directory ,(public "posts")
    :recursive t
    :publishing-function org-publish-attachment)

   ("tangles"
    :base-directory ,(src "posts")
    :publishing-directory ,(public "posts")
    :recursive t
    :publishing-function org-babel-tangle-publish)

   ;; sitemap and posts are separated because a post can use another image
   ;; and have to be excluded.  But not from the sitemap.
   ("sitemap"
    :base-directory ,(src "posts")
    :base-extension "org"
    :publishing-directory ,(public "posts")
    :publishing-function do-nothing
    :recursive t
    :exclude ,(regexp-opt '("README.org" "draft"))
    :auto-sitemap t
    :sitemap-title "Posts"
    :sitemap-filename "index.org"
    :sitemap-sort-files anti-chronologically
    :sitemap-style list
    :sitemap-format-entry ,(lambda (entry style project)
                             ;; Ugly Hack to by-pass the relative posts/ PATH issue
                             ;; copy/paste `org-publish-sitemap-default-entry'
                             ;; from ox-publish.el
                             (cond ((not (directory-name-p entry))
                                    (format "/%s/ | [[file:%s][*%s*]]"
                                            (format-time-string "%Y-%m-%d"
                                                                (org-publish-find-date entry project))
	                                    entry
	                                    (org-publish-find-title entry project)))
                                   ((eq style 'tree)
                                    ;; Return only last subdir.
                                    (file-name-nondirectory (directory-file-name entry)))
                                   (t entry)))
    :html-head ,header
    :with-creator nil)
   ("posts"
    :base-directory ,(src "posts")
    :base-extension "org"
    :publishing-directory ,(public "posts")
    :publishing-function org-html-publish-to-html
    :recursive t
    :exclude ,(regexp-opt '("README.org" "draft"
                            "2019-03-04-derivee-dual/index.org")) ;build with the rule 'derivative'
                                                                  ;using another image
    :html-head ,header
    :with-creator nil)
   ("derivative"
    :base-directory ,(src "posts/2019-03-04-derivee-dual")
    :base-extension "org"
    :publishing-directory "tmp/"
    :publishing-function org-html-publish-to-html
    :recursive t
    :html-head ,header
    :with-creator nil)
   ("site" :components ("css"
                        "bare-bone" "copy-file"
                        "sitemap"
                        "posts"
                        "extra"))))

(org-babel-do-load-languages
 'org-babel-load-languages
 '((python   . t)
   (R        . t)
   (C        . t)
   (shell    . t)
   (org      . t)
   (makefile . t)
   (scheme   . t)
   ))




(defun my/filter (pred lists)
  "TODO"
  (cl-reduce #'(lambda (results elem)
                 (if (funcall pred elem)
                     (cons elem results)
                   results))
             lists
             :initial-value '()))

(defun my/list--files (directory)
  "TODO"
  (let ((dir (file-name-as-directory directory)))
    (mapcar #'(lambda (path)
                (concat dir path))
            (my/filter #'(lambda (p)
                           (not (or (string= p ".")
                                    (string= p ".."))))
                       (directory-files dir)))))

(defun my/org-babel-execute ()
  "TODO"
  (interactive)
  (let* ((posts (assoc "posts" org-publish-project-alist))
         (dir (plist-get (cdr posts) :base-directory))

         (dirs (my/filter #'file-directory-p
                          (my/list--files dir)))

         (orgs (my/filter #'(lambda (file)
                              (string-match "\.org$" file))
                          (apply #'append
                                 (mapcar #'my/list--files dirs)))))
    (mapcar #'(lambda (file)
                (progn
                  (message (format "Org Babel execute: %s" file))
                  (find-file file)
                  (org-babel-execute-buffer)
                  (save-buffer)
                  (kill-buffer)))
            orgs)))

(defun save-buffer-with-message ()
  (save-buffer)
  (message (format "File: %s...saved." buffer-file-name)))




;; ;;; Hack
;; (my/org-babel-execute)


(provide 'publish)
;;; pkgs.el ends here
