guix environment -C -m manifest.scm -E TERM \
     -- emacs -q --batch                    \
     -l publish.el                          \
     --eval="(org-publish \"site\")"
